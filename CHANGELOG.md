# Changelog

## Unreleased

- Allow rejecting patterns like `!*.activerecord`.
- New API.

## v0.1.0

- Initial release
